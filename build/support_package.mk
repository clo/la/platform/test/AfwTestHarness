#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#
# Builds a package which is needed by a test package
#
# Replace "include $(BUILD_PACKAGE)" with "include $(BUILD_AFW_TEST_SUPPORT_PACKAGE)"
#
LOCAL_DEX_PREOPT := false
LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_PACKAGE)

afw_test_support_apks :=
$(foreach fp, $(ALL_MODULES.$(LOCAL_PACKAGE_NAME).BUILT_INSTALLED),\
  $(eval pair := $(subst :,$(space),$(fp)))\
  $(eval built := $(word 1,$(pair)))\
  $(eval installed := $(AFW_TH_TESTCASES_OUT)/$(notdir $(word 2,$(pair))))\
  $(eval $(call copy-one-file, $(built), $(installed)))\
  $(eval afw_test_support_apks += $(installed)))

$(my_register_name) : $(afw_test_support_apks)
