/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.deviceadmin;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;


/**
 * Activity to handle factory reset intent.
 */
public class FactoryResetActivity extends Activity {

    private static final String TAG = "afwtest.FactoryResetActivity";

    private static final String EXTRA_WIPE_PROTECTION_DATA = "afwtest.wipe.protection.data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ComponentName admin = new ComponentName(getApplicationContext().getPackageName(),
                AdminReceiver.class.getName());

        DevicePolicyManager devicePolicyManager =
                (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

        boolean wipeReset = getIntent().getBooleanExtra(EXTRA_WIPE_PROTECTION_DATA, false);

        String adminName = admin.flattenToString();
        if (!devicePolicyManager.isAdminActive(admin)) {
            Log.e(TAG, adminName + " is not active admin");
            Log.e(TAG, "Please run command: adb shell dpm set-active-admin " + adminName);
        } else if (wipeReset && !devicePolicyManager.isDeviceOwnerApp(getPackageName())) {
            Log.e(TAG, getPackageName() + " is not device-owner");
            Log.e(TAG, "Please run command: adb shell dpm set-device-owner " + adminName);
        } else {
            Log.d(TAG, "Active admin: " + adminName);
            if (wipeReset) {
                devicePolicyManager.wipeData(DevicePolicyManager.WIPE_RESET_PROTECTION_DATA);
            } else {
                devicePolicyManager.wipeData(0);
            }
        }
        finish();
    }
}
