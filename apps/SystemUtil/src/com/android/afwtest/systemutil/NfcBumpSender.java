/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.systemutil;

import static com.android.afwtest.common.Constants.NFC_BUMP_FILE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.afwtest.common.nfcprovisioning.NfcBumpSimulator;

import java.io.IOException;

/**
 * Activity to send a nfc bump.
 */
public class NfcBumpSender extends Activity {

    private static final String TAG = "afwtest.NfcBumpSender";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Intent intent = getIntent();
            // Gets the path of the file that containing the parameters to be sent
            // in the nfc bump.
            if (intent.hasExtra(NFC_BUMP_FILE)) {
                String bumpFileLocation = getIntent().getStringExtra(NFC_BUMP_FILE);
                NfcBumpSimulator.sendNfcBump(this, bumpFileLocation);
                Log.d(TAG, String.format("%s=%s", NFC_BUMP_FILE, bumpFileLocation));
            } else {
                Log.e(TAG, String.format("Nfc bump not sent."
                        + "Intent doesn't contain extra %s.", NFC_BUMP_FILE));
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to send Nfc Bump", e);
        }
        finish();
    }
}
