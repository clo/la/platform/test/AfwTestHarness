/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.testdpc;

import static com.android.afwtest.uiautomator.Constants.STAT_TESTDPC_WORK_PROFILE_CREATION_TIME;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_ADD_ACCOUNT_RADIO_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_NEXT_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_PKG_NAME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.concurrent.TimeUnit;

/**
 * Setup finished Add Account page in TestDpc.
 */
public final class SetupFinishedAddAccountPage extends UiPage {

    /**
     * {@link BySelector} unique to this page.
     */
    private static final BySelector TESTDPC_SETUP_FINISHED_PAGE_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "suw_layout_title")
                    .text("Setup finished");

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public SetupFinishedAddAccountPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return TESTDPC_SETUP_FINISHED_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        getProvisioningStatsLogger().stopTime(STAT_TESTDPC_WORK_PROFILE_CREATION_TIME);
        getProvisioningStatsLogger().writeStatsToFile();

        if (WidgetUtils.safeWait(getUiDevice(), TESTDPC_ADD_ACCOUNT_RADIO_BUTTON_SELECTOR,
                TimeUnit.MINUTES.toMillis(1)) == null
                || WidgetUtils.safeWait(getUiDevice(), TESTDPC_NEXT_BUTTON_SELECTOR,
                        TimeUnit.MINUTES.toMillis(1)) == null) {
            assertOnFatalAppCrash();
        }
        WidgetUtils.waitAndClick(getUiDevice(), TESTDPC_ADD_ACCOUNT_RADIO_BUTTON_SELECTOR);
        WidgetUtils.waitAndClick(getUiDevice(), TESTDPC_NEXT_BUTTON_SELECTOR);
    }
}
