/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.gms;

import static com.android.afwtest.uiautomator.Constants.GMS_CHECK_BOX_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_NEXT_BUTTON_RES_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_PKG_NAME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.Device;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * GMS Core Google Services page.
 */
public final class GoogleServicesPage extends UiPage {

    /**
     * Default UI waiting time, in milliseconds.
     */
    private static final long DEFAULT_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20);

    /**
     * {@link BySelector} unique to this page.
     */
    private static final BySelector GOOGLE_SERVICES_PAGE_SELECTOR =
            By.pkg(GMS_PKG_NAME).text("Google services");

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public GoogleServicesPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected long getLoadingTimeoutInMs() throws IOException {
        // Accessing Google server takes long time
        return 3 * super.getLoadingTimeoutInMs();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return GOOGLE_SERVICES_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {

        WidgetUtils.waitAndClick(getUiDevice(), GMS_CHECK_BOX_SELECTOR, DEFAULT_TIMEOUT_MS);

        // Scroll up so that the Next button appears.
        for (int i = 0; i < 5; ++i) {
            Device.swipeUp(getUiDevice());
            if (WidgetUtils.safeWait(getUiDevice(),
                    GMS_NEXT_BUTTON_RES_SELECTOR, TimeUnit.SECONDS.toMillis(3)) != null) {
                break;
            }
        }

        WidgetUtils.waitAndClick(getUiDevice(), GMS_NEXT_BUTTON_RES_SELECTOR, DEFAULT_TIMEOUT_MS);
    }
}