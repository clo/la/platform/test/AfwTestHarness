/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.gms;

import static com.android.afwtest.uiautomator.Constants.GMS_ADD_ACCOUNT_PAGE_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_NEXT_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_TEXT_FIELD_SELECTOR;

import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.TextField;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * GMS Core add account page.
 */
public final class AddAccountPage extends UiPage {

    /**
     * Default waiting time for widget, in milliseconds.
     */
    private static final long DEFAULT_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20);

    /**
     * Username of the account to add.
     */
    private final String mUsername;

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public AddAccountPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
        mUsername = config.getWorkAccountUsername();
    }

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     * @param customAccountKey property key of the account in {@link TestConfig}
     */
    public AddAccountPage(UiDevice uiDevice, TestConfig config, String customAccountKey) {
        super(uiDevice, config);
        mUsername = config.getProperty(customAccountKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected long getLoadingTimeoutInMs() throws IOException {
        // "Checking..." and "Loading" may take longer time than normal pages
        return 3 * super.getLoadingTimeoutInMs();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return GMS_ADD_ACCOUNT_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        TextField.enterTextAndActivateNavigationBtn(
                getUiDevice(),
                GMS_TEXT_FIELD_SELECTOR,
                mUsername,
                GMS_NEXT_BUTTON_SELECTOR);

        WidgetUtils.safeWaitAndClick(getUiDevice(), GMS_NEXT_BUTTON_SELECTOR, DEFAULT_TIMEOUT_MS);
        WidgetUtils.waitToBeGone(getUiDevice(), GMS_ADD_ACCOUNT_PAGE_SELECTOR);
    }
}
