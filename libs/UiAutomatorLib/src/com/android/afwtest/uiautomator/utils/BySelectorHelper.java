/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.utils;

import static com.android.afwtest.common.Constants.ACTION_CHECK;
import static com.android.afwtest.common.Constants.ACTION_SCROLL;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;

import com.android.afwtest.common.test.OemWidget;

/**
 * Helper class for {@link BySelector}.
 */
public final class BySelectorHelper {

    /**
     * Gets the corresponding {@link BySelector} for {@link OemWidget}
     *
     * @param widget {@link OemWidget} object
     * @return {@link BySelector} for the given {@link OemWidget}
     */
    public static BySelector getSelector(OemWidget widget) {
        BySelector selector = By.enabled(true);

        if (!widget.getText().isEmpty()) {
            selector.text(widget.getText());
        }

        if (!widget.getDescription().isEmpty()) {
            selector.desc(widget.getDescription());
        }

        if (!widget.getResourceId().isEmpty()) {
            selector.res(widget.getResourceId());
        }

        if (!widget.getPackage().isEmpty()) {
            selector.pkg(widget.getPackage());
        }

        if (!widget.getClassName().isEmpty()) {
            selector.clazz(widget.getClassName());
        }

        if (widget.getAction().equals(ACTION_SCROLL)) {
            selector.scrollable(true);
        } else if (widget.getAction().equals(ACTION_CHECK)) {
            selector.checkable(true).checked(false);
        } else {
            selector.clickable(true);
        }

        return selector;
    }
}
