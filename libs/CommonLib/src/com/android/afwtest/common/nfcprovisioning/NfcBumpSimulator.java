/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common.nfcprovisioning;

import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_SIGNATURE_CHECKSUM;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_SKIP_ENCRYPTION;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_WIFI_PASSWORD;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_WIFI_SECURITY_TYPE;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_WIFI_SSID;
import static android.app.admin.DevicePolicyManager.MIME_TYPE_PROVISIONING_NFC;
import static android.nfc.NfcAdapter.ACTION_NDEF_DISCOVERED;
import static com.android.afwtest.common.Constants.KEY_DEVICE_ADMIN_PKG_CHECKSUM;
import static com.android.afwtest.common.Constants.KEY_DEVICE_ADMIN_PKG_SIGNATURE_HASH;
import static com.android.afwtest.common.Preconditions.checkNotEmpty;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;

import com.android.afwtest.common.test.TestConfig;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Simulates Nfc Bump to start device owner provisioning.
 */
public final class NfcBumpSimulator {

    private static final String TAG = "afwtest.NfcBumpSimulator";

    /**
     * Sends NFC bump to initiate device owner provisioning.
     *
     * @param context {@link Context} object
     * @param propsConfigFile {@link Properties} configuration file containing NFC bump properties.
     * @return Device admin package name after provisioning completes successfully
     */
    public static String sendNfcBump(Context context, String propsConfigFile) throws IOException {

        TestConfig testConfig = TestConfig.get(propsConfigFile);

        Properties bumpProps = new Properties();
        String deviceAdminPkgName = checkNotEmpty(testConfig.getDeviceAdminPkgName());
        bumpProps.put(EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME, deviceAdminPkgName);
        bumpProps.put(EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION,
                checkNotEmpty(testConfig.getDeviceAdminPkgLocation()));

        String signatureHash = testConfig.getDeviceAdminPkgSignatureHash("");
        String checksum = testConfig.getDeviceAdminPkgChecksum("");
        if (signatureHash.isEmpty() && checksum.isEmpty()) {
            throw new RuntimeException(String.format("Neither %s nor %s is specified in file %s",
                    KEY_DEVICE_ADMIN_PKG_SIGNATURE_HASH,
                    KEY_DEVICE_ADMIN_PKG_CHECKSUM,
                    propsConfigFile));
        }

        if (!signatureHash.isEmpty()) {
            bumpProps.put(EXTRA_PROVISIONING_DEVICE_ADMIN_SIGNATURE_CHECKSUM, signatureHash);
        }

        // Add checksum if it's not empty because OEM L devices use SHA1 checksum only.
        if (!checksum.isEmpty()) {
            bumpProps.put(EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM, checksum);
        }

        String wifiSsid = checkNotEmpty(testConfig.getWifiSsid());
        // Make sure to surround SSID with double quotes.
        if (!wifiSsid.startsWith("\"") || !wifiSsid.endsWith("\"")) {
            wifiSsid = "\"" + wifiSsid + "\"";
        }
        bumpProps.put(EXTRA_PROVISIONING_WIFI_SSID, wifiSsid);

        bumpProps.put(EXTRA_PROVISIONING_WIFI_SECURITY_TYPE, testConfig.getWifiSecurityType(""));
        bumpProps.put(EXTRA_PROVISIONING_WIFI_PASSWORD, testConfig.getWifiPassword(""));

        // Skip encryption.
        bumpProps.put(EXTRA_PROVISIONING_SKIP_ENCRYPTION, "true");

        sendNfcBump(context, bumpProps);

        // Return expected device admin package name
        return deviceAdminPkgName;
    }

    /**
     * Sends NFC bump to initiate device owner provisioning.
     *
     * @param context {@link Context} object.
     * @param props {@link Properties} object contains NFC bump properties.
     */
    public static void sendNfcBump(Context context, Properties props) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        props.store(stream, "AFW NFC provisioning");
        NdefRecord record = NdefRecord
                .createMime(MIME_TYPE_PROVISIONING_NFC, stream.toByteArray());
        NdefMessage ndfMsg = new NdefMessage(new NdefRecord[]{record});

        Intent intent = new Intent(ACTION_NDEF_DISCOVERED);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setType(MIME_TYPE_PROVISIONING_NFC);
        intent.putExtra(NfcAdapter.EXTRA_NDEF_MESSAGES, new NdefMessage[]{ndfMsg});

        context.startActivity(intent);
    }
}
