/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;

/**
 * A {@link ITargetPreparer} that sets enable-root option of the testing device.
 */
@OptionClass(alias = "afw-test-adb-root-option")
public class AfwTestAdbRootOptionPreparer implements ITargetCleaner {

    @Option(name = "enable-root-option",
            description = "Whether to enable adb root option on the testing device.")
    private boolean mEnableAdbRootOption = true;

    /**
     * Original adb root option.
     */
    private Boolean mOriginalRootOption = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo) throws TargetSetupError,
            DeviceNotAvailableException {
        mOriginalRootOption = device.getOptions().isEnableAdbRoot();
        device.getOptions().setEnableAdbRoot(mEnableAdbRootOption);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {
        if (mOriginalRootOption != null) {
            device.getOptions().setEnableAdbRoot(mOriginalRootOption);
        }
    }
}
