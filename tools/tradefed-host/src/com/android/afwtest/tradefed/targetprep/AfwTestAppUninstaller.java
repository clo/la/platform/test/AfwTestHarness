/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * A {@link ITargetPreparer} that helps uninstalling apps before or after the test.
 */
@OptionClass(alias="afw-test-app-uninstaller")
public class AfwTestAppUninstaller implements ITargetCleaner {

    @Option(name = "before-test", description = "packages to uninstall before test")
    private List<String> mPackageNamesBeforeTest = new LinkedList<>();

    @Option(name = "after-test", description = "packages to uninstall after test")
    private List<String> mPackageNamesAfterTest = new LinkedList<>();

    @Option(name = "reboot-before-uninstall",
            description = "if reboot the device before uinstallation")
    private boolean mRebootBeforeUninstall = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, DeviceNotAvailableException {
        String result = uninstall(device, mPackageNamesBeforeTest);

        if (result != null) {
            throw new TargetSetupError(String.format("Failed to uninstall %s on %s",
                    result, device.getSerialNumber()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {

        String result = uninstall(device, mPackageNamesAfterTest);

        if (result != null) {
            throw new RuntimeException(String.format("Failed to uninstall %s on %s",
                    result, device.getSerialNumber()));
        }
    }

    /**
     * Uninstalls list of packages.
     *
     * @param device the testing device
     * @param packages packages to uninstall
     * @return {@code null} if all given packages are uninstalled, or the name of the first package
     * that failed to be uninstalled
     */
    private String uninstall(ITestDevice device, List<String> packages)
            throws DeviceNotAvailableException {
        Set<String> installedPackages = device.getInstalledPackageNames();
        installedPackages.retainAll(packages);

        if (installedPackages.isEmpty()) {
            return null;
        }

        if (mRebootBeforeUninstall) {
            device.reboot();
        }

        for (String pkgName : installedPackages) {
            String result = device.uninstallPackage(pkgName);
            if (result != null) {
                return pkgName;
            }

            CLog.i(String.format("Successfully uninstalled %s on %s",
                    pkgName, device.getSerialNumber()));
        }

        return null;
    }
}
